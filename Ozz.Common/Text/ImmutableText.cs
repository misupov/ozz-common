﻿using System;
using System.Text;
using Ozz.Common.Collections.Immutable;
using Ozz.Common.Strings;

namespace Ozz.Common.Text
{
    public class ImmutableText
    {
        private const int MaxChunkSize = 4096;

        private readonly AugmentedTree<StringEx, long, StringMeasurer> _chunks;

        public ImmutableText()
        {
            _chunks = AugmentedTree<StringEx, long, StringMeasurer>.EmptyNode;
        }

        private ImmutableText(AugmentedTree<StringEx, long, StringMeasurer> chunks)
        {
            _chunks = chunks;
        }

        public long ChunksCount
        {
            get { return _chunks.Count; }
        }

        public long Length
        {
            get { return _chunks.TotalMeasure; }
        }

        public ImmutableText Clear()
        {
            return new ImmutableText(AugmentedTree<StringEx, long, StringMeasurer>.EmptyNode);
        }

        public ImmutableText Append(StringEx text)
        {
            return Insert(Length, text);
        }

        public ImmutableText Insert(long offset, StringEx text)
        {
            if (text.Length == 0)
            {
                return this;
            }
            if (text.Length <= MaxChunkSize)
            {
                return new ImmutableText(InsertInternal(_chunks, offset, text));
            }
            
            var result = _chunks;
            long innerOffset = 0;
            while (true)
            {
                var length = Math.Min(MaxChunkSize, text.Length - innerOffset);
                if (length == 0)
                {
                    break;
                }
                var chunk = text.Substring(innerOffset, length);
                result = InsertInternal(result, offset + innerOffset, chunk);
                innerOffset += length;
            }
            return new ImmutableText(result);
        }

        public ImmutableText Remove(long offset, long length)
        {
            var result = _chunks;
            var itemInfo = result.GetItemInfoAtDistance(offset);
            if (itemInfo.InnerDistance > 0)
            {
                var toRemove = itemInfo.Item.Length - itemInfo.InnerDistance;
                if (length >= toRemove)
                {
                    result = result.ReplaceAt(itemInfo.Index, itemInfo.Item.Substring(0, (int) itemInfo.InnerDistance));
                    length -= toRemove;
                }
                else
                {
                    result = result.ReplaceAt(itemInfo.Index,
                        StringEx.Join(
                            itemInfo.Item.Substring(0, (int) itemInfo.InnerDistance),
                            itemInfo.Item.Substring((int) (itemInfo.InnerDistance + length))));
                    return new ImmutableText(result);
                }
            }
            while (length > 0)
            {
                itemInfo = result.GetItemInfoAtDistance(offset);
                if (length >= itemInfo.Item.Length)
                {
                    result = result.RemoveAt(itemInfo.Index);
                    length -= itemInfo.Item.Length;
                }
                else
                {
                    result = result.ReplaceAt(itemInfo.Index, itemInfo.Item.Substring((int) length));
                    break;
                }
            }
            return new ImmutableText(result);
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (var chunk in _chunks)
            {
                sb.Append(chunk);
            }
            return sb.ToString();
        }

        private AugmentedTree<StringEx, long, StringMeasurer> InsertInternal(AugmentedTree<StringEx, long, StringMeasurer> tree, long offset, StringEx text)
        {
            if (offset == tree.TotalMeasure)
            {
                if (tree.IsEmpty)
                {
                    return tree.Add(text);
                }
                var lastChunk = tree[tree.Count - 1];
                return text.Length + lastChunk.Length <= MaxChunkSize
                    ? tree.ReplaceAt(tree.Count - 1, StringEx.Join(lastChunk, text))
                    : tree.Add(text);
            }

            var itemInfo = tree.GetItemInfoAtDistance(offset);
            if (itemInfo.InnerDistance == 0)
            {
                var right = itemInfo.Item;
                if (itemInfo.Index - 1 >= 0)
                {
                    var left = tree[itemInfo.Index - 1];
                    if (left.Length < right.Length && left.Length + text.Length <= MaxChunkSize)
                    {
                        return tree.ReplaceAt(itemInfo.Index - 1, StringEx.Join(left, text));
                    }
                }
                if (right.Length + text.Length <= MaxChunkSize)
                {
                    return tree.ReplaceAt(itemInfo.Index, StringEx.Join(text, itemInfo.Item));
                }
                return tree.Insert(itemInfo.Index, text);
            }

            var leftPart = itemInfo.Item.Substring(0, (int)itemInfo.InnerDistance);
            var rightPart = itemInfo.Item.Substring((int)itemInfo.InnerDistance);

            if (text.Length + itemInfo.Item.Length <= MaxChunkSize)
            {
                return tree
                    .RemoveAt(itemInfo.Index)
                    .Insert(itemInfo.Index, StringEx.Join(leftPart, text, rightPart));
            }
            if (leftPart.Length < rightPart.Length && leftPart.Length + text.Length <= MaxChunkSize)
            {
                return tree
                    .RemoveAt(itemInfo.Index)
                    .Insert(itemInfo.Index, rightPart)
                    .Insert(itemInfo.Index, StringEx.Join(leftPart, text));
            }
            if (rightPart.Length + text.Length <= MaxChunkSize)
            {
                return tree
                    .RemoveAt(itemInfo.Index)
                    .Insert(itemInfo.Index, StringEx.Join(text, rightPart))
                    .Insert(itemInfo.Index, leftPart);
            }
            return tree
                .RemoveAt(itemInfo.Index)
                .Insert(itemInfo.Index, rightPart)
                .Insert(itemInfo.Index, text)
                .Insert(itemInfo.Index, leftPart);
        }

        public StringEx GetSubstring(long offset, long length)
        {
            if (offset >= _chunks.TotalMeasure)
            {
                return "";
            }

            if (offset + length >= _chunks.TotalMeasure)
            {
                var firstItemInfo = _chunks.GetItemInfoAtDistance(offset);
                var firstSubStr = _chunks[firstItemInfo.Index].Substring(firstItemInfo.InnerDistance);
                if (firstItemInfo.Index == _chunks.Count - 1)
                {
                    return firstSubStr;
                }
                var substrings = new StringEx[_chunks.Count - firstItemInfo.Index];
                substrings[0] = firstSubStr;
                for (var i = firstItemInfo.Index + 1; i < _chunks.Count; i++)
                {
                    substrings[i - firstItemInfo.Index] = _chunks[i];
                }
                return new CompoundString(substrings);
            }
            else
            {
                var firstItemInfo = _chunks.GetItemInfoAtDistance(offset);
                var lastItemInfo = _chunks.GetItemInfoAtDistance(offset + length);
                if (firstItemInfo.Index == lastItemInfo.Index)
                {
                    return _chunks[firstItemInfo.Index].Substring(
                        firstItemInfo.InnerDistance,
                        lastItemInfo.InnerDistance - firstItemInfo.InnerDistance);
                }
                if (firstItemInfo.Index == lastItemInfo.Index - 1)
                {
                    return new CompoundString(
                        _chunks[firstItemInfo.Index].Substring(firstItemInfo.InnerDistance),
                        _chunks[lastItemInfo.Index].Substring(0, lastItemInfo.InnerDistance));
                }
                var substrings = new StringEx[lastItemInfo.Index - firstItemInfo.Index + 1];
                substrings[0] = _chunks[firstItemInfo.Index].Substring(firstItemInfo.InnerDistance);
                for (var i = firstItemInfo.Index + 1; i < lastItemInfo.Index; i++)
                {
                    substrings[i - firstItemInfo.Index] = _chunks[i];
                }
                substrings[substrings.Length - 1] = _chunks[lastItemInfo.Index].Substring(0, lastItemInfo.InnerDistance);
                return new CompoundString(substrings);
            }
        }
    }
}