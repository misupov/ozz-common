﻿using Ozz.Common.Collections.Immutable;
using Ozz.Common.Strings;

namespace Ozz.Common.Text
{
    internal class StringMeasurer : IMeasurer<StringEx, long>
    {
        public long Nil { get { return 0; } }

        public long GetMeasure(StringEx item)
        {
            return item.Length;
        }

        public long Add(long measure1, long measure2)
        {
            return measure1 + measure2;
        }

        public long Subtract(long measure1, long measure2)
        {
            return measure1 - measure2;
        }

        public int Compare(long measure1, long measure2)
        {
            return measure1.CompareTo(measure2);
        }
    }
}