﻿using System.Collections;
using System.Collections.Generic;
using Ozz.Common.Internal;

namespace Ozz.Common.Strings
{
    public class RepeatableCharSequence : StringEx
    {
        private readonly char _char;
        private readonly long _count;

        public RepeatableCharSequence(char c, long count)
        {
            Requires.Range(count >= 0, "count");

            _char = c;
            _count = count;
        }

        public override long Length
        {
            get
            {
                return _count;
            }
        }

        public override char this[long index]
        {
            get
            {
                return _char;
            }
        }

        public override StringEx Substring(long startIndex, long length)
        {
            if (startIndex == 0 && length == Length)
            {
                return this;
            }

            if (length == 0)
            {
                return ClrString.Empty;
            }

            return new RepeatableCharSequence(_char, length);
        }

        public override StringEx Substring(long startIndex)
        {
            return Substring(startIndex, _count - startIndex);
        }

        public override IEnumerator<char> GetEnumerator()
        {
            for (int i = 0; i < _count; i++)
            {
                yield return _char;
            }
        }

        public override string ToString()
        {
            return new string(_char, (int) _count);
        }
    }
}