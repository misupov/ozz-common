namespace Ozz.Common.Strings
{
    public struct LineLength
    {
        public LineLength(long length, LineBreak lineBreak) : this()
        {
            Length = length;
            LineBreak = lineBreak;
        }

        public long Length { get; private set; }
        public LineBreak LineBreak { get; private set; }
    }
}