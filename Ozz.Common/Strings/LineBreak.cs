namespace Ozz.Common.Strings
{
    public enum LineBreak
    {
        NoBreak,
        CrLf,                   // \u000d\u000a
        Cr,                     // \u000d
        Lf,                     // \u000a
        VerticalTab,            // \u000b
        FormFeed,               // \u000c
        NextLine,               // \u0085
        LineSeparator,          // \u2028
        ParagraphSeparator,     // \u2029
    }
}