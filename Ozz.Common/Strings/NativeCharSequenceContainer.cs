﻿using System;
using System.Runtime.InteropServices;
using Ozz.Common.Internal;

namespace Ozz.Common.Strings
{
    public unsafe class NativeCharSequenceContainer : IDisposable
    {
        private GCHandle _handle;
        private readonly NativeCharSequence _string;

        public NativeCharSequenceContainer(char[] charArray)
        {
            Requires.NotNull(charArray, "charArray");

            _handle = GCHandle.Alloc(charArray, GCHandleType.Pinned);
            var startAddress = (char*)_handle.AddrOfPinnedObject();
            var length = charArray.Length;
            _string = new NativeCharSequence(startAddress, length, _handle);
        }

        ~NativeCharSequenceContainer()
        {
            Dispose();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            if (_handle.IsAllocated)
            {
                _handle.Free();
            }
        }

        public StringEx String
        {
            get { return _string; }
        }
    }
}