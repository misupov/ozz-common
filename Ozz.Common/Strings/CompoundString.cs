﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ozz.Common.Internal;

namespace Ozz.Common.Strings
{
    internal sealed class CompoundString : StringEx
    {
        private readonly List<long> _offsets;
        private readonly List<StringEx> _strings;
        private readonly long _length;
        private int _deepness;

        public CompoundString(IEnumerable<StringEx> strings)
        {
            var collection = strings as ICollection<StringEx>;
            var capacity = collection != null ? collection.Count : 4;
            _offsets = new List<long>(capacity);
            _strings = new List<StringEx>(capacity);
            _length = strings.Where(s => s.Length > 0).Aggregate(0L, (current, source) => AddString(source, current));
            ++_deepness;
        }

        public CompoundString(params StringEx[] strings)
            : this(strings.AsEnumerable())
        {
        }

        public CompoundString(StringEx string1, StringEx string2)
        {
            var originalString = string1 as CompoundString;
            if (originalString != null)
            {
                _offsets = originalString._offsets;
                _strings = originalString._strings;
                _deepness = originalString.Deepness - 1;
                _length = originalString.Length;
                _length = AddString(string2, _length);
            }
            else
            {
                _offsets = new List<long>();
                _strings = new List<StringEx>();
                _length = AddString(string1, _length);
                _length = AddString(string2, _length);
            }
            ++_deepness;
        }

        public CompoundString(StringEx string1, StringEx string2, StringEx string3)
        {
            var originalString = string1 as CompoundString;
            if (originalString != null)
            {
                _offsets = originalString._offsets;
                _strings = originalString._strings;
                _deepness = originalString.Deepness - 1;
                _length = originalString.Length;
                _length = AddString(string2, _length);
                _length = AddString(string3, _length);
            }
            else
            {
                _offsets = new List<long>();
                _strings = new List<StringEx>();
                _length = AddString(string1, _length);
                _length = AddString(string2, _length);
                _length = AddString(string3, _length);
            }
            ++_deepness;
        }

        private long AddString(StringEx str, long offs)
        {
            var composedString = str as CompoundString;
            if (composedString != null && composedString.Deepness == 3)
            {
                foreach (var substring in composedString._strings)
                {
                    _strings.Add(substring);
                    _offsets.Add(offs);
                    offs += substring.Length;
                }
                _deepness = Math.Max(_deepness, 2);
            }
            else
            {
                _strings.Add(str);
                _offsets.Add(offs);
                offs += str.Length;
                _deepness = Math.Max(_deepness, composedString != null ? composedString._deepness : 1);
            }
            return offs;
        }

        public int Deepness
        {
            get { return _deepness; }
        }

        public override long Length
        {
            get { return _length; }
        }

        public override char this[long index]
        {
            get
            {
                Requires.Range(index >= 0 && index < Length, "index");

                var idx = _offsets.BinarySearch(index);
                if (idx < 0)
                {
                    idx = ~idx - 1;
                }
                return _strings[idx][index - _offsets[idx]];
            }
        }

        public override StringEx Substring(long startIndex, long length)
        {
            if (startIndex == 0 && length == _length)
            {
                return this;
            }

            if (length == 0)
            {
                return ClrString.Empty;
            }

            Requires.Range(length >= 0, "length");
            Requires.Range(startIndex >= 0 && startIndex + length <= _length, "startIndex");

            var sIdx = _offsets.BinarySearch(startIndex);
            if (sIdx < 0)
            {
                sIdx = ~sIdx - 1;
            }
            var eIdx = _offsets.BinarySearch(startIndex + length - 1);
            if (eIdx < 0)
            {
                eIdx = ~eIdx - 1;
            }
            if (sIdx == eIdx)
            {
                return _strings[sIdx].Substring(startIndex - _offsets[sIdx], length);
            }
            var result = new List<StringEx>(eIdx - sIdx + 1);
            result.Add(_strings[sIdx].Substring(startIndex - _offsets[sIdx]));
            for (int i = sIdx + 1; i < eIdx; i++)
            {
                result.Add(_strings[i]);
            }
            result.Add(_strings[eIdx].Substring(0, startIndex + length - _offsets[eIdx]));
            return new CompoundString(result);
        }

        public override StringEx Substring(long startIndex)
        {
            return Substring(startIndex, _length - startIndex);
        }

        public override IEnumerator<char> GetEnumerator()
        {
            return _strings.SelectMany(str => str).GetEnumerator();
        }

        public override string ToString()
        {
            return string.Join("", _strings.Select(s => s.ToString()));
        }
    }
}