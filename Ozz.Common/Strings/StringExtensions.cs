﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Ozz.Common.Strings
{
    public static class StringExtensions
    {
        [DebuggerStepThrough]
        public static StringEx ToStringEx(this string str)
        {
            if (ReferenceEquals(str, null))
            {
                return null;
            }

            if (str.Length == 0)
            {
                return ClrString.Empty;
            }
            if (str.Length == 1)
            {
                return SingleCharString.Create(str[0]);
            }
            return new ClrString(str, 0, str.Length);
        }

        public static long GetOffsetOfFirstNonWhitespaceChar(this StringEx str)
        {
            for (var i = 0L; i < str.Length; i++)
            {
                if (!char.IsWhiteSpace(str[i]))
                {
                    return i;
                }
            }
            return 0;
        }
    }
}