﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Ozz.Common.Internal;

namespace Ozz.Common.Strings
{
    internal sealed class ClrString : StringEx
    {
        public static readonly ClrString Empty = new ClrString(string.Empty, 0, 0);

        private readonly string _str;
        private readonly int _index;
        private readonly int _length;

        [DebuggerHidden]
        public ClrString(string str, int index, int length)
        {
            _str = str;
            _index = index;
            _length = length;
        }

        [DebuggerHidden]
        public override long Length
        {
            get
            {
                return _length;
            }
        }

        [DebuggerHidden]
        public override char this[long index]
        {
            get
            {
                Requires.Range(index >= 0, "index");
                Requires.Range(index < _length, "index");
                return _str[(int) (_index + index)];
            }
        }

        public override StringEx Substring(long startIndex, long length)
        {
            if (startIndex == 0 && length == _length)
            {
                return this;
            }

            if (length == 0)
            {
                return Empty;
            }

            Requires.Range(length >= 0, "length");
            Requires.Range(startIndex >= 0 && startIndex + length <= _length, "startIndex");

            if (_str.Length > 10 && _str.Length < 2*length)
            {
                return new ClrString(_str.Substring((int) (_index + startIndex), (int) length), 0, (int) length);
            }
            
            return new ClrString(_str, (int) (_index + startIndex), (int) length);
        }

        public override StringEx Substring(long startIndex)
        {
            return Substring(startIndex, _length - startIndex);
        }

        public override IEnumerator<char> GetEnumerator()
        {
            var lastIndex = _index + _length;
            for (int i = _index; i < lastIndex; i++)
            {
                yield return _str[i];
            }
        }

        public override string ToString()
        {
            return _str.Substring(_index, _length);
        }
    }
}