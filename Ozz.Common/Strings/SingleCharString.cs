﻿using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using Ozz.Common.Internal;

namespace Ozz.Common.Strings
{
    internal class SingleCharString : StringEx
    {
        private static readonly ConcurrentDictionary<char, SingleCharString> Cache = new ConcurrentDictionary<char, SingleCharString>();
        private readonly char _char;

        private SingleCharString(char c)
        {
            _char = c;
        }

        public static SingleCharString Create(char c)
        {
            return Cache.GetOrAdd(c, ch => new SingleCharString(ch));
        }

        public override long Length
        {
            get { return 1; }
        }

        public override char this[long index]
        {
            get
            {
                Requires.Range(index == 0, "index");
                return _char;
            }
        }

        public override StringEx Substring(long startIndex, long length)
        {
            Requires.Range(startIndex >= 0 && startIndex <= 1, "startIndex");
            Requires.Range(length >= 0 && length <= 1, "length");
            Requires.Range(startIndex + length <= 1, "startIndex");

            return length == 0 ? (StringEx) ClrString.Empty : this;
        }

        public override StringEx Substring(long startIndex)
        {
            Requires.Range(startIndex >= 0 && startIndex <= 1, "startIndex");
            return startIndex == 1 ? (StringEx)ClrString.Empty : this;
        }

        public override IEnumerator<char> GetEnumerator()
        {
            yield return _char;
        }

        public override string ToString()
        {
            return _char.ToString(CultureInfo.InvariantCulture);
        }
    }
}