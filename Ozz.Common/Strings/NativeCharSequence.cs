﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Ozz.Common.Internal;

namespace Ozz.Common.Strings
{
    internal unsafe sealed class NativeCharSequence : StringEx
    {
        public static readonly StringEx Empty = new NativeCharSequence();

        private readonly char* _startAddress;
        private readonly long _length;
        private readonly GCHandle _handle;

        private NativeCharSequence()
        {
        }

        public NativeCharSequence(char* startAddress, long length, GCHandle handle)
        {
            _startAddress = startAddress;
            _length = length;
            _handle = handle;
        }

        public override StringEx Substring(long startIndex, long length)
        {
            if (startIndex == 0 && length == Length)
            {
                return this;
            }

            if (length == 0)
            {
                return ClrString.Empty;
            }

            Requires.Range(length >= 0, "length");
            Requires.Range(startIndex >= 0 && startIndex + length <= _length, "startIndex");

            return new NativeCharSequence(_startAddress + startIndex, length, _handle);
        }

        public override StringEx Substring(long startIndex)
        {
            return Substring(startIndex, _length - startIndex);
        }

        public override long Length { get { return _length; } }

        public override char this[long index]
        {
            get { return *(_startAddress + index); }
        }

        public override IEnumerator<char> GetEnumerator()
        {
            for (int i = 0; i < Length; i++)
            {
                yield return this[i];
            }
        }

        public override string ToString()
        {
            return _length == 0 ? string.Empty : new string(_startAddress, 0, (int)_length);
        }
    }
}