﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Ozz.Common.Strings
{
    public abstract class StringEx : IEnumerable<char>
    {
        public abstract long Length { get; }

        public abstract char this[long index] { get; }

        public abstract StringEx Substring(long startIndex, long length);

        public abstract StringEx Substring(long startIndex);
        
        public abstract IEnumerator<char> GetEnumerator();
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        [DebuggerStepThrough]
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj is StringEx && StringsComparer.AreEqual(this, (StringEx)obj);
        }

        [DebuggerStepThrough]
        public override int GetHashCode()
        {
            return StringsComparer.GetHashCode(this);
        }

        [DebuggerStepThrough]
        public static StringEx Join(StringEx string1, StringEx string2)
        {
            return new CompoundString(string1, string2);
        }

        [DebuggerStepThrough]
        public static StringEx Join(StringEx string1, StringEx string2, StringEx string3)
        {
            return new CompoundString(string1, string2, string3);
        }

        [DebuggerStepThrough]
        public static StringEx Join(params StringEx[] strings)
        {
            return new CompoundString(strings);
        }

        [DebuggerStepThrough]
        public static StringEx Join(IEnumerable<StringEx> strings)
        {
            return new CompoundString(strings);
        }

        [DebuggerStepThrough]
        public static implicit operator StringEx(string str)
        {
            return str.ToStringEx();
        }

        [DebuggerStepThrough]
        public static bool operator ==(StringEx str1, StringEx str2)
        {
            return StringsComparer.AreEqual(str1, str2);
        }

        [DebuggerStepThrough]
        public static bool operator !=(StringEx str1, StringEx str2)
        {
            return !(str1 == str2);
        }
    }
}