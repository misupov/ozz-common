﻿namespace Ozz.Common.Strings
{
    internal static class StringsComparer
    {
        public static bool AreEqual(StringEx str1, StringEx str2)
        {
            if (ReferenceEquals(str1, null) && ReferenceEquals(str2, null))
            {
                return true;
            }

            if (ReferenceEquals(str1, null))
            {
                return false;
            }

            if (ReferenceEquals(str2, null))
            {
                return false;
            }

            if (ReferenceEquals(str1, str2))
            {
                return true;
            }

            var e1 = str1.GetEnumerator();
            var e2 = str2.GetEnumerator();
            
            bool m1;
            bool m2;

            while (true)
            {
                m1 = e1.MoveNext();
                m2 = e2.MoveNext();
                if (!m1 || !m2)
                {
                    break;
                }
                if (e1.Current != e2.Current)
                {
                    return false;
                }
            }

            return m1 == m2;
        }

        public static int GetHashCode(StringEx str)
        {
            var enumerator = str.GetEnumerator();
            int num = 352654597;
            int num2 = num;
            long i;
            for (i = str.Length; i > 1; i -= 2)
            {
                enumerator.MoveNext();
                num = ((num << 5) + num + (num >> 27) ^ enumerator.Current);
                enumerator.MoveNext();
                num2 = ((num2 << 5) + num2 + (num2 >> 27) ^ enumerator.Current);
            }
            if (i > 0)
            {
                enumerator.MoveNext();
                num = ((num << 5) + num + (num >> 27) ^ enumerator.Current);
            }
            return num + num2 * 1566083941;
        }
    }
}