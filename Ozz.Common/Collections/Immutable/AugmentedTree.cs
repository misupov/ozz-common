﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Ozz.Common.Internal;

namespace Ozz.Common.Collections.Immutable
{
    public static class AugmentedTree
    {
        public static AugmentedTree<TItem, TItem, SelfMeasurer<TItem>> Create<TItem>()
            where TItem : struct, IComparable<TItem>
        {
            return AugmentedTree<TItem, TItem, SelfMeasurer<TItem>>.EmptyNode;
        }

        public static AugmentedTree<TItem, TMeasure, EmbeddedTypesMeasurer<TItem, TMeasure>> Create<TItem, TMeasure>()
            where TItem : IMeasurable<TMeasure>
            where TMeasure : struct, IComparable<TMeasure>
        {
            return AugmentedTree<TItem, TMeasure, EmbeddedTypesMeasurer<TItem, TMeasure>>.EmptyNode;
        }

        public static AugmentedTree<TItem, TMeasure, TItemsMeasurer> Create<TItem, TMeasure, TItemsMeasurer>()
            where TMeasure : struct, IComparable<TMeasure>
            where TItemsMeasurer : IMeasurer<TItem, TMeasure>, new()
        {
            return AugmentedTree<TItem, TMeasure, TItemsMeasurer>.EmptyNode;
        }

        public static AugmentedTree<TItem, TItem, SelfMeasurer<TItem>> FromEnumerable<TItem>(IEnumerable<TItem> enumerable)
            where TItem : struct, IComparable<TItem>
        {
            return AugmentedTree<TItem, TItem, SelfMeasurer<TItem>>.FromEnumerable(enumerable);
        }

        public static AugmentedTree<TItem, TMeasure, EmbeddedTypesMeasurer<TItem, TMeasure>> FromEnumerable<TItem, TMeasure>(IEnumerable<TItem> enumerable)
            where TItem : IMeasurable<TMeasure>
            where TMeasure : struct, IComparable<TMeasure>
        {
            return AugmentedTree<TItem, TMeasure, EmbeddedTypesMeasurer<TItem, TMeasure>>.FromEnumerable(enumerable);
        }

        public static AugmentedTree<TItem, TMeasure, TItemsMeasurer> FromEnumerable<TItem, TMeasure, TItemsMeasurer>(IEnumerable<TItem> enumerable)
            where TMeasure : struct, IComparable<TMeasure>
            where TItemsMeasurer : IMeasurer<TItem, TMeasure>, new()
        {
            return AugmentedTree<TItem, TMeasure, TItemsMeasurer>.FromEnumerable(enumerable);
        }

        public static AugmentedTree<TItem, TItem, SelfMeasurer<TItem>> FromEnumerable<TItem>(IEnumerable<TItem> enumerable, int start, int length)
        where TItem : struct, IComparable<TItem>
        {
            return AugmentedTree<TItem, TItem, SelfMeasurer<TItem>>.FromEnumerable(enumerable, start, length);
        }

        public static AugmentedTree<TItem, TMeasure, EmbeddedTypesMeasurer<TItem, TMeasure>> FromEnumerable<TItem, TMeasure>(IEnumerable<TItem> enumerable, int start, int length)
            where TItem : IMeasurable<TMeasure>
            where TMeasure : struct, IComparable<TMeasure>
        {
            return AugmentedTree<TItem, TMeasure, EmbeddedTypesMeasurer<TItem, TMeasure>>.FromEnumerable(enumerable, start, length);
        }

        public static AugmentedTree<TItem, TMeasure, TItemsMeasurer> FromEnumerable<TItem, TMeasure, TItemsMeasurer>(IEnumerable<TItem> enumerable, int start, int length)
            where TMeasure : struct, IComparable<TMeasure>
            where TItemsMeasurer : IMeasurer<TItem, TMeasure>, new()
        {
            return AugmentedTree<TItem, TMeasure, TItemsMeasurer>.FromEnumerable(enumerable, start, length);
        }
    }

    [DebuggerDisplay("Count = {Count}")]
    public class AugmentedTree<TItem, TMeasure, TItemMeasurer> : IEnumerable<TItem>
        where TItemMeasurer : IMeasurer<TItem, TMeasure>, new()
    {
        private const byte HeightMask = 0x3f;
        private const byte ValueMeasured = 0x40;
        private const byte TotalMeasured = 0x80;

        private static readonly TItemMeasurer Measurer;
        internal static readonly AugmentedTree<TItem, TMeasure, TItemMeasurer> EmptyNode;

        static AugmentedTree()
        {
            Measurer = new TItemMeasurer();
            EmptyNode = new AugmentedTree<TItem, TMeasure, TItemMeasurer>();
        }

        private readonly TItem _value;
        private TMeasure _valueMeasure;
        private TMeasure _totalMeasure;
        private readonly AugmentedTree<TItem, TMeasure, TItemMeasurer> _left;
        private readonly AugmentedTree<TItem, TMeasure, TItemMeasurer> _right;
        private readonly long _count;
        
        // Highest two bits are used as flags indicating whether value/total measures are calculated.
        private byte _heightAndMeasureState;

        internal static AugmentedTree<TItem, TMeasure, TItemMeasurer> FromEnumerable(IEnumerable<TItem> enumerable)
        {
            // ReSharper disable PossibleMultipleEnumeration
            Requires.NotNull(enumerable, "enumerable");
            return FromEnumerableInternal(enumerable);
            // ReSharper restore PossibleMultipleEnumeration
        }

        internal static AugmentedTree<TItem, TMeasure, TItemMeasurer> FromEnumerable(IEnumerable<TItem> enumerable, int start, int length)
        {
            // ReSharper disable PossibleMultipleEnumeration
            Requires.NotNull(enumerable, "enumerable");
            Requires.Range(start >= 0, "start");
            Requires.Range(length >= 0, "length");
            return FromEnumerableInternal(enumerable, start, length);
            // ReSharper restore PossibleMultipleEnumeration
        }

        private AugmentedTree()
        {
            _heightAndMeasureState = ValueMeasured | TotalMeasured;
            _totalMeasure = _valueMeasure = Measurer.Nil;
        }

        private AugmentedTree(TItem value, 
            AugmentedTree<TItem, TMeasure, TItemMeasurer> left,
            AugmentedTree<TItem, TMeasure, TItemMeasurer> right)
        {
            Requires.NotNull(left, "left");
            Requires.NotNull(right, "right");
            _value = value;
            _left = left;
            _right = right;
            _heightAndMeasureState = (byte)(1 + Math.Max(left.Height, right.Height));
            _count = 1 + left.Count + right.Count;
        }

        public TItem Value
        {
            get { return _value; }
        }

        public int Height
        {
            get { return _heightAndMeasureState & HeightMask; }
        }

        public AugmentedTree<TItem, TMeasure, TItemMeasurer> Left
        {
            get { return _left; }
        }

        public AugmentedTree<TItem, TMeasure, TItemMeasurer> Right
        {
            get { return _right; }
        }

        public bool IsEmpty
        {
            get { return _left == null; }
        }

        public long Count
        {
            get { return _count; }
        }

        public TMeasure ValueMeasure
        {
            get
            {
                lock (this)
                {
                    if ((_heightAndMeasureState & ValueMeasured) == 0)
                    {
                        _valueMeasure = Measurer.GetMeasure(Value);
                        _heightAndMeasureState |= ValueMeasured;
                    }
                    return _valueMeasure;
                }
            }
        }

        public TMeasure TotalMeasure
        {
            get
            {
                lock (this)
                {
                    if ((_heightAndMeasureState & TotalMeasured) == 0)
                    {
                        _totalMeasure = Measurer.Add(ValueMeasure, Measurer.Add(Left.TotalMeasure, Right.TotalMeasure));
                        _heightAndMeasureState |= TotalMeasured;
                    }
                    return _totalMeasure;
                }
            }
        }

        public TItem this[long index]
        {
            get
            {
                Requires.Range(index >= 0 && index < Count, "index");
                return GetItemInternal(index);
            }
        }

        public AugmentedTree<TItem, TMeasure, TItemMeasurer> Add(TItem item)
        {
            return InsertInternal(Count, new AugmentedTree<TItem, TMeasure, TItemMeasurer>(item, EmptyNode, EmptyNode));
        }

        public AugmentedTree<TItem, TMeasure, TItemMeasurer> Add(AugmentedTree<TItem, TMeasure, TItemMeasurer> subTree)
        {
            return InsertInternal(Count, subTree);
        }

        public AugmentedTree<TItem, TMeasure, TItemMeasurer> Insert(long index, TItem item)
        {
            Requires.Range(index >= 0 && index <= Count, "index");
            return InsertInternal(index, new AugmentedTree<TItem, TMeasure, TItemMeasurer>(item, EmptyNode, EmptyNode));
        }

        public AugmentedTree<TItem, TMeasure, TItemMeasurer> Insert(long index, AugmentedTree<TItem, TMeasure, TItemMeasurer> subTree)
        {
            Requires.Range(index >= 0 && index <= Count, "index");
            return InsertInternal(index, subTree);
        }

        public AugmentedTree<TItem, TMeasure, TItemMeasurer> RemoveAt(long index)
        {
            Requires.Range(index >= 0 && index < Count, "index");
            AugmentedTree<TItem, TMeasure, TItemMeasurer> tree;
            if (index == _left.Count)
            {
                if (_right.IsEmpty && _left.IsEmpty)
                {
                    tree = EmptyNode;
                }
                else if (_right.IsEmpty)
                {
                    tree = _left;
                }
                else if (_left.IsEmpty)
                {
                    tree = _right;
                }
                else
                {
                    var node = _right;
                    while (!node._left.IsEmpty)
                    {
                        node = node._left;
                    }
                    var right = _right.RemoveAt(0);
                    tree = node.Mutate(_left, right);
                }
            }
            else
            {
                tree = index < _left.Count
                    ? Mutate(_left.RemoveAt(index), null)
                    : Mutate(null, _right.RemoveAt(index - _left.Count - 1));
            }
            return tree.IsEmpty ? tree : MakeBalanced(tree);
        }

        public AugmentedTree<TItem, TMeasure, TItemMeasurer> ReplaceAt(long index, TItem value)
        {
            Requires.Range(index >= 0 && index < Count, "index");
            return ReplaceAtInternal(index, value);
        }

        public ItemInfo<TItem, TMeasure> GetItemInfoAtDistance(TMeasure distance)
        {
            Requires.Range(Measurer.Compare(distance, Measurer.Nil) >= 0 && Measurer.Compare(distance, TotalMeasure) < 0, "index");

            long index;
            TItem item;
            TMeasure outerOffset;
            TMeasure innerOffset;
            GetItemAtDistanceInternal(distance, out index, out item, out outerOffset, out innerOffset);
            return new ItemInfo<TItem, TMeasure>(index, item, outerOffset, innerOffset);
        }

        public ItemInfo<TItem, TMeasure> GetItemInfoAtIndex(long index)
        {
            Requires.Range(index >= 0 && index < Count, "index");
            TItem item;
            TMeasure offset;
            GetItemAtIndexInternal(index, out item, out offset);
            return new ItemInfo<TItem, TMeasure>(index, item, offset, Measurer.Nil);
        }

        private TItem GetItemInternal(long index)
        {
            if (index < _left.Count)
                return _left.GetItemInternal(index);
            if (index > _left.Count)
                return _right.GetItemInternal(index - _left.Count - 1);
            return Value;
        }

        private AugmentedTree<TItem, TMeasure, TItemMeasurer> InsertInternal(long index, AugmentedTree<TItem, TMeasure, TItemMeasurer> subTree)
        {
            if (IsEmpty)
            {
                return subTree;
            }
            var tree = index <= _left.Count
                ? Mutate(_left.InsertInternal(index, subTree), null)
                : Mutate(null, _right.InsertInternal(index - _left.Count - 1, subTree));
            return MakeBalanced(tree);
        }

        private AugmentedTree<TItem, TMeasure, TItemMeasurer> ReplaceAtInternal(long index, TItem value)
        {
            if (index == _left.Count)
            {
                return Mutate(value);
            }
            return index < _left.Count
                ? Mutate(_left.ReplaceAt(index, value), null)
                : Mutate(null, _right.ReplaceAt(index - _left.Count - 1, value));
        }

        private void GetItemAtDistanceInternal(TMeasure distance, out long index, out TItem resultItem, out TMeasure outerOffset, out TMeasure innerOffset)
        {
            var m1 = Measurer.Subtract(distance, _left.TotalMeasure);

            if (Measurer.Compare(m1, Measurer.Nil) < 0)
            {
                _left.GetItemAtDistanceInternal(distance, out index, out resultItem, out outerOffset, out innerOffset);
                return;
            }

            var measure = Measurer.Add(_left.TotalMeasure, ValueMeasure);
            if (Measurer.Compare(distance, measure) >= 0)
            {
                _right.GetItemAtDistanceInternal(Measurer.Subtract(distance, measure), out index, out resultItem, out outerOffset, out innerOffset);
                index += _left.Count + 1;
                outerOffset = Measurer.Add(measure, outerOffset);
                return;
            }

            index = _left.Count;
            resultItem = Value;
            outerOffset = _left.TotalMeasure;
            innerOffset = Measurer.Subtract(distance, outerOffset);
        }

        private void GetItemAtIndexInternal(long index, out TItem resultItem, out TMeasure offset)
        {
            if (index < _left.Count)
            {
                _left.GetItemAtIndexInternal(index, out resultItem, out offset);
                return;
            }
            if (index > _left.Count)
            {
                _right.GetItemAtIndexInternal(index - _left.Count - 1, out resultItem, out offset);
                offset = Measurer.Add(offset, Measurer.Add(_left.TotalMeasure, ValueMeasure));
                return;
            }
            resultItem = Value;
            offset = _left.TotalMeasure;
        }

        private static AugmentedTree<TItem, TMeasure, TItemMeasurer> RotateLeft(AugmentedTree<TItem, TMeasure, TItemMeasurer> tree)
        {
            Requires.NotNull(tree, "tree");
            if (tree.Right.IsEmpty)
                return tree;
            var node = tree._right;
            return node.Mutate(tree.Mutate(null, node._left), null);
        }

        private static AugmentedTree<TItem, TMeasure, TItemMeasurer> RotateRight(AugmentedTree<TItem, TMeasure, TItemMeasurer> tree)
        {
            Requires.NotNull(tree, "tree");
            if (tree._left.IsEmpty)
                return tree;
            var node = tree._left;
            return node.Mutate(null, tree.Mutate(node._right, null));
        }

        private static AugmentedTree<TItem, TMeasure, TItemMeasurer> DoubleLeft(AugmentedTree<TItem, TMeasure, TItemMeasurer> tree)
        {
            Requires.NotNull(tree, "tree");
            return tree._right.IsEmpty ? tree : RotateLeft(tree.Mutate(null, RotateRight(tree._right)));
        }

        private static AugmentedTree<TItem, TMeasure, TItemMeasurer> DoubleRight(AugmentedTree<TItem, TMeasure, TItemMeasurer> tree)
        {
            Requires.NotNull(tree, "tree");
            return tree._left.IsEmpty ? tree : RotateRight(tree.Mutate(RotateLeft(tree._left), null));
        }

        private static int Balance(AugmentedTree<TItem, TMeasure, TItemMeasurer> tree)
        {
            Requires.NotNull(tree, "tree");
            return tree.Right.Height - tree.Left.Height;
        }

        private static bool IsRightHeavy(AugmentedTree<TItem, TMeasure, TItemMeasurer> tree)
        {
            Requires.NotNull(tree, "tree");
            return Balance(tree) >= 2;
        }

        private static bool IsLeftHeavy(AugmentedTree<TItem, TMeasure, TItemMeasurer> tree)
        {
            Requires.NotNull(tree, "tree");
            return Balance(tree) <= -2;
        }

        private static AugmentedTree<TItem, TMeasure, TItemMeasurer> MakeBalanced(AugmentedTree<TItem, TMeasure, TItemMeasurer> tree)
        {
            Requires.NotNull(tree, "tree");
            if (IsRightHeavy(tree))
            {
                return !IsLeftHeavy(tree._right) ? RotateLeft(tree) : DoubleLeft(tree);
            }

            if (!IsLeftHeavy(tree))
            {
                return tree;
            }

            if (!IsRightHeavy(tree._left))
            {
                return RotateRight(tree);
            }
            
            return DoubleRight(tree);
        }

        private AugmentedTree<TItem, TMeasure, TItemMeasurer> Mutate(AugmentedTree<TItem, TMeasure, TItemMeasurer> left, AugmentedTree<TItem, TMeasure, TItemMeasurer> right)
        {
            return new AugmentedTree<TItem, TMeasure, TItemMeasurer>(Value, left ?? _left, right ?? _right);
        }

        private AugmentedTree<TItem, TMeasure, TItemMeasurer> Mutate(TItem value)
        {
            return new AugmentedTree<TItem, TMeasure, TItemMeasurer>(value, _left, _right);
        }

        private static AugmentedTree<TItem, TMeasure, TItemMeasurer> FromListInternal(IList<TItem> items, int start, int length)
        {
            if (length == 0)
            {
                return EmptyNode;
            }
            var leftLength = (length - 1) / 2;
            var rightLength = length - 1 - leftLength;
            var left = FromListInternal(items, start, rightLength);
            var right = FromListInternal(items, start + rightLength + 1, leftLength);
            return new AugmentedTree<TItem, TMeasure, TItemMeasurer>(items[start + rightLength], left, right);
        }

        private static AugmentedTree<TItem, TMeasure, TItemMeasurer> FromEnumerableInternal(IEnumerable<TItem> items, int start, int length)
        {
            var list = items as IList<TItem>;
            if (list != null)
            {
                return FromListInternal(list, start, length);
            }

            return FromEnumerableInternal(items.Skip(start).Take(length));
        }

        private static AugmentedTree<TItem, TMeasure, TItemMeasurer> FromEnumerableInternal(IEnumerable<TItem> items)
        {
            var list = items as IList<TItem>;
            if (list != null)
            {
                return FromListInternal(list, 0, list.Count);
            }

            var result = EmptyNode;
            var buffer = new List<TItem>(1024); // must be 2^n for better balancing
            var enumerator = items.GetEnumerator();
            var hasItems = false;
            do
            {
                var idx = 0;
                buffer.Clear();
                while (idx < buffer.Capacity && (hasItems = enumerator.MoveNext()))
                {
                    buffer.Add(enumerator.Current);
                    idx++;
                }
                result = result.Add(FromListInternal(buffer, 0, idx));
            } while (hasItems);
            return result;
        }

        public IEnumerator<TItem> GetEnumerator()
        {
            return new TreeEnumerator<TItem, TMeasure, TItemMeasurer>(this);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class ItemInfo<TItem, TMeasure>
    {
        internal ItemInfo(long index, TItem item, TMeasure outerDistance, TMeasure innerDistance)
        {
            Index = index;
            Item = item;
            OuterDistance = outerDistance;
            InnerDistance = innerDistance;
        }

        public long Index { get; private set; }
        public TItem Item { get; private set; }
        public TMeasure OuterDistance { get; private set; }
        public TMeasure InnerDistance { get; private set; }

        public override string ToString()
        {
            return string.Format("Index: {0}, Item: {1}, OuterDistance: {2}, InnerDistance: {3}", Index, Item, OuterDistance, InnerDistance);
        }
    }
}