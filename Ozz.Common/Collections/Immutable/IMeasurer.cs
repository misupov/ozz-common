﻿namespace Ozz.Common.Collections.Immutable
{
    public interface IMeasurer<in TItem, TMeasure>
    {
        TMeasure Nil { get; }
        TMeasure GetMeasure(TItem item);
        TMeasure Add(TMeasure measure1, TMeasure measure2);
        TMeasure Subtract(TMeasure measure1, TMeasure measure2);
        int Compare(TMeasure measure1, TMeasure measure2);
    }
}