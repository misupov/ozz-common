using System;
using System.Collections;
using System.Collections.Generic;

namespace Ozz.Common.Collections.Immutable
{
    internal class TreeEnumerator<TItem, TMeasure, TItemMeasurer> : IEnumerator<TItem>
        where TItemMeasurer : IMeasurer<TItem, TMeasure>, new()
    {
        private readonly Stack<AugmentedTree<TItem, TMeasure, TItemMeasurer>> _stack = new Stack<AugmentedTree<TItem, TMeasure, TItemMeasurer>>();
        private AugmentedTree<TItem, TMeasure, TItemMeasurer> _tree;
        private AugmentedTree<TItem, TMeasure, TItemMeasurer> _current;
        private AugmentedTree<TItem, TMeasure, TItemMeasurer> _next;

        internal TreeEnumerator(AugmentedTree<TItem, TMeasure, TItemMeasurer> tree)
        {
            _tree = tree;
            _current = null;
            _next = tree;
        }

        public void Dispose()
        {
            _tree = null;
        }

        public bool MoveNext()
        {
            ThrowIfDisposed();
            _current = _next;
            while (!_current.IsEmpty)
            {
                _stack.Push(_current);
                _current = _current.Left;
            }
            if (_stack.Count > 0)
            {
                _current = _stack.Pop();
                _next = !_current.Right.IsEmpty
                    ? _current.Right
                    : AugmentedTree<TItem, TMeasure, TItemMeasurer>.EmptyNode;
                return true;
            }
            return false;
        }

        public void Reset()
        {
            ThrowIfDisposed();
            _next = _tree;
            _current = null;
            _stack.Clear();
        }

        public TItem Current
        {
            get
            {
                ThrowIfDisposed();
                if (_current == null)
                {
                    throw new InvalidOperationException();
                }
                return _current.Value;
            }
        }

        object IEnumerator.Current
        {
            get { return Current; }
        }

        private void ThrowIfDisposed()
        {
            if (_tree == null)
            {
                throw new ObjectDisposedException(GetType().FullName);
            }
        }
    }
}