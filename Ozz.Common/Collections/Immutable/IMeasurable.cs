﻿namespace Ozz.Common.Collections.Immutable
{
    public interface IMeasurable<out TMeasure>
    {
        TMeasure GetMeasure();
    }
}