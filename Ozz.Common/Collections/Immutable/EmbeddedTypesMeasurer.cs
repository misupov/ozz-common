using System;
using System.Linq.Expressions;

namespace Ozz.Common.Collections.Immutable
{
    public class EmbeddedTypesMeasurer<TItem, TMeasure> : IMeasurer<TItem, TMeasure>
        where TItem : IMeasurable<TMeasure>
        where TMeasure : IComparable<TMeasure>
    {
        private readonly Func<TMeasure, TMeasure, TMeasure> _add;
        private readonly Func<TMeasure, TMeasure, TMeasure> _subtract;
        private readonly Func<TMeasure, TMeasure, int> _compare;

        public EmbeddedTypesMeasurer()
        {
            var parameter1 = Expression.Parameter(typeof (TMeasure));
            var parameter2 = Expression.Parameter(typeof (TMeasure));

            var addExpression = Expression.Add(parameter1, parameter2);
            _add = Expression.Lambda<Func<TMeasure, TMeasure, TMeasure>>(addExpression, parameter1, parameter2).Compile();

            var subtractExpression = Expression.Subtract(parameter1, parameter2);
            _subtract = Expression.Lambda<Func<TMeasure, TMeasure, TMeasure>>(subtractExpression, parameter1, parameter2).Compile();

            Expression<Func<TMeasure, int>> compareToMethodCallExpression = measure => measure.CompareTo(default(TMeasure));
            var compareToMethodInfo = ((MethodCallExpression) compareToMethodCallExpression.Body).Method;
            var compareToExpression = Expression.Call(parameter1, compareToMethodInfo, parameter2);
            _compare = Expression.Lambda<Func<TMeasure, TMeasure, int>>(compareToExpression, parameter1, parameter2).Compile();
        }

        public TMeasure Nil { get { return default(TMeasure); } }

        public TMeasure GetMeasure(TItem item)
        {
            return item.GetMeasure();
        }

        public TMeasure Add(TMeasure measure1, TMeasure measure2)
        {
            return _add(measure1, measure2);
        }

        public TMeasure Subtract(TMeasure measure1, TMeasure measure2)
        {
            return _subtract(measure1, measure2);
        }

        public int Compare(TMeasure measure1, TMeasure measure2)
        {
            return _compare(measure1, measure2);
        }
    }
}