using System;
using System.Linq.Expressions;

namespace Ozz.Common.Collections.Immutable
{
    public class SelfMeasurer<TItem> : IMeasurer<TItem, TItem>
        where TItem : struct, IComparable<TItem>
    {
        private readonly Func<TItem, TItem, TItem> _add;
        private readonly Func<TItem, TItem, TItem> _subtract;
        private readonly Func<TItem, TItem, int> _compare;

        public SelfMeasurer()
        {
            var parameter1 = Expression.Parameter(typeof (TItem));
            var parameter2 = Expression.Parameter(typeof (TItem));

            var addExpression = Expression.Add(parameter1, parameter2);
            _add = Expression.Lambda<Func<TItem, TItem, TItem>>(addExpression, parameter1, parameter2).Compile();

            var subtractExpression = Expression.Subtract(parameter1, parameter2);
            _subtract = Expression.Lambda<Func<TItem, TItem, TItem>>(subtractExpression, parameter1, parameter2).Compile();

            Expression<Func<TItem, int>> compareToMethodCallExpression = measure => measure.CompareTo(default(TItem));
            var compareToMethodInfo = ((MethodCallExpression) compareToMethodCallExpression.Body).Method;
            var compareToExpression = Expression.Call(parameter1, compareToMethodInfo, parameter2);
            _compare = Expression.Lambda<Func<TItem, TItem, int>>(compareToExpression, parameter1, parameter2).Compile();
        }

        public TItem Nil { get { return default(TItem); } }
        
        public TItem GetMeasure(TItem item)
        {
            return item;
        }

        public TItem Add(TItem measure1, TItem measure2)
        {
            return _add(measure1, measure2);
        }

        public TItem Subtract(TItem measure1, TItem measure2)
        {
            return _subtract(measure1, measure2);
        }

        public int Compare(TItem measure1, TItem measure2)
        {
            return _compare(measure1, measure2);
        }
    }
}