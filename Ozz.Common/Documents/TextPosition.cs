namespace Ozz.Common.Documents
{
    public class TextPosition
    {
        public TextPosition(long line, long column)
        {
            Line = line;
            Column = column;
        }

        public long Line { get; private set; }
        public long Column { get; private set; }
    }
}