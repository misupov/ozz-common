﻿using Ozz.Common.Strings;

namespace Ozz.Common.Documents
{
    public interface IReadOnlyDocument
    {
        long Length { get; }

        long LinesCount { get; }

        TextPosition GetPositionByOffset(long offset);

        long GetOffsetByPosition(TextPosition position);

        StringEx GetSubstring(long offset, long length);

        StringEx GetLine(long lineIndex);
    }
}