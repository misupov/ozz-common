using System.Collections.Generic;
using System.Threading;
using Ozz.Common.Collections.Immutable;
using Ozz.Common.Internal;
using Ozz.Common.Strings;

namespace Ozz.Common.Documents
{
    internal class ImmutableLinesIndex
    {
        private readonly AugmentedTree<LineLength, long, LineLengthMeasurer> _lines = AugmentedTree<LineLength, long, LineLengthMeasurer>.EmptyNode;

        public ImmutableLinesIndex()
        {
        }

        private ImmutableLinesIndex(AugmentedTree<LineLength, long, LineLengthMeasurer> lines)
        {
            _lines = lines;
        }

        public long LinesCount
        {
            get { return _lines.Count; }
        }

        public LineLength this[long lineIndex]
        {
            get { return _lines[lineIndex]; }
        }

        public ImmutableLinesIndex Insert(long offset, StringEx str)
        {
            return Insert(offset, str, CancellationToken.None);
        }

        public ImmutableLinesIndex Insert(long offset, StringEx str, CancellationToken cancellationToken)
        {
            Requires.Range(offset >= 0 && offset <= _lines.TotalMeasure, "offset");

            if (cancellationToken.IsCancellationRequested)
            {
                return this;
            }

            var lines = _lines;
            var enumerator = GetLinesLengthsAndBreaks(str, cancellationToken).GetEnumerator();

            if (!enumerator.MoveNext())
            {
                return this;
            }

            if (offset == lines.TotalMeasure)
            {
                if (lines.Count == 0 || lines[lines.Count - 1].LineBreak != LineBreak.NoBreak)
                {
                    lines = lines.Add(enumerator.Current);
                    while (enumerator.MoveNext())
                    {
                        lines = lines.Add(enumerator.Current);
                    }

                    return cancellationToken.IsCancellationRequested ? this : new ImmutableLinesIndex(lines);
                }

                var lastLine = lines[lines.Count - 1];
                lines = lines.ReplaceAt(lines.Count - 1, new LineLength(lastLine.Length + enumerator.Current.Length, enumerator.Current.LineBreak));
                while (enumerator.MoveNext())
                {
                    lines = lines.Add(enumerator.Current);
                }
                return cancellationToken.IsCancellationRequested ? this : new ImmutableLinesIndex(lines);
            }

            var itemInfo = _lines.GetItemInfoAtDistance(offset);

            var splitCrLfBreak = itemInfo.Item.LineBreak == LineBreak.CrLf && itemInfo.InnerDistance == itemInfo.Item.Length - 1;
            if (splitCrLfBreak)
            {
                var index = itemInfo.Index;
                if (enumerator.Current.Length == 1 && enumerator.Current.LineBreak == LineBreak.Lf)
                {
                    lines = lines.ReplaceAt(index, new LineLength(itemInfo.Item.Length, LineBreak.CrLf));
                }
                else
                {
                    lines = lines
                        .ReplaceAt(index, new LineLength(itemInfo.Item.Length - 1, LineBreak.Cr))
                        .Insert(++index, enumerator.Current);
                }
                while (enumerator.MoveNext())
                {
                    lines = lines.Insert(++index, enumerator.Current);
                }
                var lastStrLine = lines[index];
                switch (lastStrLine.LineBreak)
                {
                    case LineBreak.Cr:
                        lines = lines.ReplaceAt(index, new LineLength(lastStrLine.Length + 1, LineBreak.CrLf));
                        break;
                    case LineBreak.NoBreak:
                        lines = lines.ReplaceAt(index, new LineLength(lastStrLine.Length + 1, LineBreak.Lf));
                        break;
                    default:
                        lines = lines.Insert(++index, new LineLength(1, LineBreak.Lf));
                        break;
                }
            }
            else
            {
                var index = itemInfo.Index;
                lines = lines.ReplaceAt(index, new LineLength(itemInfo.InnerDistance + enumerator.Current.Length, enumerator.Current.LineBreak));
                while (enumerator.MoveNext())
                {
                    lines = lines.Insert(++index, enumerator.Current);
                }
                var lastStrLine = lines[index];
                switch (lastStrLine.LineBreak)
                {
                    case LineBreak.NoBreak:
                        lines = lines.ReplaceAt(index, new LineLength(lastStrLine.Length + itemInfo.Item.Length - itemInfo.InnerDistance, itemInfo.Item.LineBreak));
                        break;
                    default:
                        lines = lines.Insert(++index, new LineLength(itemInfo.Item.Length - itemInfo.InnerDistance, itemInfo.Item.LineBreak));
                        break;
                }
            }
            return cancellationToken.IsCancellationRequested ? this : new ImmutableLinesIndex(lines);
        }

        public ImmutableLinesIndex Remove(int offset, int length)
        {
            return Remove(offset, length, CancellationToken.None);
        }

        public ImmutableLinesIndex Remove(int offset, int length, CancellationToken cancellationToken)
        {
            Requires.Range(offset >= 0 && offset + length <= _lines.TotalMeasure, "offset");

            if (cancellationToken.IsCancellationRequested)
            {
                return this;
            }

            var lines = _lines;

            var firstItemInfo = lines.GetItemInfoAtDistance(offset);
            var firstItemIndex = firstItemInfo.Index;

            if (offset + length == lines.TotalMeasure)
            {
                var count = lines.Count;
                for (var i = firstItemIndex + 1; i < count; i++)
                {
                    lines = lines.RemoveAt(firstItemIndex + 1);
                }
                if (firstItemInfo.InnerDistance > 0)
                {
                    lines = lines.ReplaceAt(firstItemIndex, GetLeftSubLine(firstItemInfo.Item, firstItemInfo.InnerDistance));
                }
                else
                {
                    lines = lines.RemoveAt(firstItemIndex);
                }
                return cancellationToken.IsCancellationRequested ? this : new ImmutableLinesIndex(lines);
            }

            var lastItemInfo = lines.GetItemInfoAtDistance(offset + length);

            for (var i = firstItemIndex + 1; i < lastItemInfo.Index; i++)
            {
                lines = lines.RemoveAt(firstItemIndex + 1);
            }

            var firstLine = GetLeftSubLine(firstItemInfo.Item, firstItemInfo.InnerDistance);
            var lastLine = GetRightSubLine(lastItemInfo.Item, lastItemInfo.InnerDistance);

            if (firstItemInfo.Index == lastItemInfo.Index)
            {
                lines = lines.ReplaceAt(firstItemIndex, new LineLength(firstLine.Length + lastLine.Length, lastLine.LineBreak));
            }
            else
            {
                lines = lines
                    .ReplaceAt(firstItemIndex, firstLine)
                    .ReplaceAt(firstItemIndex + 1, lastLine);

                if (firstLine.LineBreak == LineBreak.NoBreak)
                {
                    lines = lines
                        .ReplaceAt(firstItemIndex, new LineLength(firstLine.Length + lastLine.Length, lastLine.LineBreak))
                        .RemoveAt(firstItemIndex + 1);
                }
                else if (lastLine.Length == 1 && lastLine.LineBreak == LineBreak.Lf)
                {
                    lines = lines
                        .ReplaceAt(firstItemIndex, new LineLength(firstLine.Length + 1, LineBreak.CrLf))
                        .RemoveAt(firstItemIndex + 1);
                }
            }

            return cancellationToken.IsCancellationRequested ? this : new ImmutableLinesIndex(lines);
        }

        private LineLength GetLeftSubLine(LineLength lineLength, long offset)
        {
            var splitCrLf = lineLength.LineBreak == LineBreak.CrLf && offset == lineLength.Length - 1;

            return new LineLength(offset, splitCrLf ? LineBreak.Cr : LineBreak.NoBreak);
        }

        private LineLength GetRightSubLine(LineLength lineLength, long offset)
        {
            var splitCrLf = lineLength.LineBreak == LineBreak.CrLf && offset == lineLength.Length - 1;
            
            return new LineLength(lineLength.Length - offset, splitCrLf ? LineBreak.Lf : lineLength.LineBreak);
        }

        public TextPosition GetPositionByOffset(long offset)
        {
            var itemInfo = _lines.GetItemInfoAtDistance(offset);
            return new TextPosition(itemInfo.Index, itemInfo.InnerDistance);
        }

        public long GetOffsetByPosition(TextPosition position)
        {
            var itemInfo = _lines.GetItemInfoAtIndex(position.Line);
            return itemInfo.OuterDistance + itemInfo.InnerDistance;
        }

        private static IEnumerable<LineLength> GetLinesLengthsAndBreaks(StringEx str, CancellationToken cancellationToken)
        {
            var length = str.Length;
            var lineLength = 0L;
            var isCr = false;

            for (long i = 0; i < length; i++)
            {
                if (i % 10000 == 0 && cancellationToken.IsCancellationRequested)
                {
                    yield break;
                }
                var c = str[i];
                lineLength++;
                if (c == '\u000d')
                {
                    if (i == length - 1 || str[i + 1] != '\u000a')
                    {
                        yield return new LineLength(lineLength, LineBreak.Cr);
                        lineLength = 0;
                    }
                    isCr = true;
                    continue;
                }

                switch (c)
                {
                    case '\u000a':
                        yield return new LineLength(lineLength, isCr ? LineBreak.CrLf : LineBreak.Lf);
                        lineLength = 0;
                        break;
                    case '\u000b':
                        yield return new LineLength(lineLength, LineBreak.VerticalTab);
                        lineLength = 0;
                        break;
                    case '\u000c':
                        yield return new LineLength(lineLength, LineBreak.FormFeed);
                        lineLength = 0;
                        break;
                    case '\u0085':
                        yield return new LineLength(lineLength, LineBreak.NextLine);
                        lineLength = 0;
                        break;
                    case '\u2028':
                        yield return new LineLength(lineLength, LineBreak.LineSeparator);
                        lineLength = 0;
                        break;
                    case '\u2029':
                        yield return new LineLength(lineLength, LineBreak.ParagraphSeparator);
                        lineLength = 0;
                        break;
                }
                isCr = false;
            }

            if (lineLength > 0)
            {
                yield return new LineLength(lineLength, LineBreak.NoBreak);
            }
        }

        private class LineLengthMeasurer : IMeasurer<LineLength, long>
        {
            public long Nil
            {
                get { return 0; }
            }

            public long GetMeasure(LineLength item)
            {
                return item.Length;
            }

            public long Add(long measure1, long measure2)
            {
                return measure1 + measure2;
            }

            public long Subtract(long measure1, long measure2)
            {
                return measure1 - measure2;
            }

            public int Compare(long measure1, long measure2)
            {
                return measure1.CompareTo(measure2);
            }
        }
    }
}