﻿using System;
using System.Threading;
using Ozz.Common.Strings;

namespace Ozz.Common.Documents
{
    public class Document
    {
        private volatile ImmutableDocument _document = new ImmutableDocument();

        public event DocumentChangedEventHandler DocumentChanged;

        public long Length
        {
            get
            {
                var document = _document;
                return document.Length;
            }
        }

        public void Append(StringEx str, CancellationToken cancellationToken)
        {
            Insert(_document.Length, str, cancellationToken);
        }

        public void Insert(long offset, StringEx str, CancellationToken cancellationToken)
        {
            var oldDocument = _document;
            var newDocument = oldDocument.Insert(offset, str, cancellationToken);
            if (!cancellationToken.IsCancellationRequested)
            {
                if (Interlocked.CompareExchange(ref _document, newDocument, oldDocument) != oldDocument)
                {
                    throw new InvalidOperationException();
                }
                RaiseDocumentChanged(new DocumentChangedEventArgs(oldDocument, newDocument, offset, str.Length, 0));
            }
        }

        public void Remove(int offset, int length, CancellationToken cancellationToken)
        {
            var oldDocument = _document;
            var newDocument = oldDocument.Remove(offset, length, cancellationToken);
            if (!cancellationToken.IsCancellationRequested)
            {
                if (Interlocked.CompareExchange(ref _document, newDocument, oldDocument) != oldDocument)
                {
                    throw new InvalidOperationException();
                }
                RaiseDocumentChanged(new DocumentChangedEventArgs(oldDocument, newDocument, offset, 0, length));
            }
        }

        public void Replace(int offset, int length, StringEx str, CancellationToken cancellationToken)
        {
            var oldDocument = _document;
            var newDocument = oldDocument.Remove(offset, length, cancellationToken).Insert(offset, str, cancellationToken);
            if (!cancellationToken.IsCancellationRequested)
            {
                if (Interlocked.CompareExchange(ref _document, newDocument, oldDocument) != oldDocument)
                {
                    throw new InvalidOperationException();
                }
                RaiseDocumentChanged(new DocumentChangedEventArgs(oldDocument, newDocument, offset, str.Length, length));
            }
        }

        public IReadOnlyDocument GetReadOnlyCopy()
        {
            return _document;
        }

        public override string ToString()
        {
            return _document.ToString();
        }

        private void RaiseDocumentChanged(DocumentChangedEventArgs args)
        {
            var handler = DocumentChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }
    }

    public delegate void DocumentChangedEventHandler(object sender, DocumentChangedEventArgs args);

    public class DocumentChangedEventArgs
    {
        public IReadOnlyDocument OldDocument { get; set; }
        public IReadOnlyDocument NewDocument { get; set; }
        public long Offset { get; set; }
        public long AddedLength { get; set; }
        public long RemovedLength { get; set; }

        public DocumentChangedEventArgs(IReadOnlyDocument oldDocument, IReadOnlyDocument newDocument, long offset, long addedLength, long removedLength)
        {
            OldDocument = oldDocument;
            NewDocument = newDocument;
            Offset = offset;
            AddedLength = addedLength;
            RemovedLength = removedLength;
        }
    }
}