﻿using System.Text;
using System.Threading;
using Ozz.Common.Strings;
using Ozz.Common.Text;

namespace Ozz.Common.Documents
{
    internal class ImmutableDocument : IReadOnlyDocument
    {
        private readonly ImmutableText _text;
        private readonly ImmutableLinesIndex _linesIndex;

        public ImmutableDocument()
        {
            _text = new ImmutableText();
            _linesIndex = new ImmutableLinesIndex();
        }

        private ImmutableDocument(ImmutableText text, ImmutableLinesIndex linesIndex)
        {
            _text = text;
            _linesIndex = linesIndex;
        }

        public long Length
        {
            get { return _text.Length; }
        }

        public ImmutableDocument Append(StringEx str, CancellationToken cancellationToken)
        {
            return cancellationToken.IsCancellationRequested ? this : Insert(_text.Length, str, cancellationToken);
        }

        public ImmutableDocument Insert(long offset, StringEx str, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return this;
            }
            
            var newText = _text.Insert(offset, str);
            var newLinesIndex = _linesIndex.Insert(offset, str, cancellationToken);
            return cancellationToken.IsCancellationRequested ? this : new ImmutableDocument(newText, newLinesIndex);
        }

        public ImmutableDocument Remove(int offset, int length, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return this;
            }

            var newText = _text.Remove(offset, length);
            var newLinesIndex = _linesIndex.Remove(offset, length, cancellationToken);
            return cancellationToken.IsCancellationRequested ? this : new ImmutableDocument(newText, newLinesIndex);
        }

        public ImmutableDocument Replace(int offset, int length, StringEx str, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return this;
            }

            var newText = _text.Remove(offset, length).Insert(offset, str);
            var newLinesIndex = _linesIndex.Remove(offset, length, cancellationToken).Insert(offset, str, cancellationToken);
            return cancellationToken.IsCancellationRequested ? this : new ImmutableDocument(newText, newLinesIndex);
        }

        public long LinesCount
        {
            get { return _linesIndex.LinesCount; }
        }

        public TextPosition GetPositionByOffset(long offset)
        {
            return _linesIndex.GetPositionByOffset(offset);
        }

        public long GetOffsetByPosition(TextPosition position)
        {
            return _linesIndex.GetOffsetByPosition(position);
        }

        public StringEx GetSubstring(long offset, long length)
        {
            return _text.GetSubstring(offset, length);
        }

        public StringEx GetLine(long lineIndex)
        {
            var offset = _linesIndex.GetOffsetByPosition(new TextPosition(lineIndex, 0));
            var length = _linesIndex[lineIndex].Length;
            return GetSubstring(offset, length);
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            for (int i = 0; i < _linesIndex.LinesCount; i++)
            {
                var offset = _linesIndex.GetOffsetByPosition(new TextPosition(i, 0));
                var length = _linesIndex[i].Length;
                var lineBreak = _linesIndex[i].LineBreak;
                var strLen = (int)length;
                if (lineBreak == LineBreak.CrLf)
                {
                    strLen -= 2;
                }
                else if (lineBreak != LineBreak.NoBreak)
                {
                    strLen--;
                }
                sb.Append(_text.ToString().Substring((int)offset, strLen));
                sb.Append(" (").Append(offset).Append(", ").Append(length).Append(", ").Append(lineBreak).Append(")");
                if (lineBreak != LineBreak.NoBreak)
                {
                    sb.AppendLine();
                }
            }
            return sb.ToString();
        }
    }
}