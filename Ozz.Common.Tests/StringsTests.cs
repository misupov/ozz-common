﻿using NUnit.Framework;
using Ozz.Common.Strings;

namespace Ozz.Common.Tests
{
    [TestFixture]
    public class StringsTests
    {
        [Test]
        public void GetHashCodeTest()
        {
            StringEx s1 = "123456789";
            StringEx s2 = new string('1', 1) + "23456789";
            StringEx s3 = StringEx.Join("1", "2", "3456789");

            Assert.AreEqual(s1.GetHashCode(), s2.GetHashCode());
            Assert.AreEqual(s2.GetHashCode(), s3.GetHashCode());
        }

        [Test]
        public void SingleCharStringsAreTheSame()
        {
            var s1 = new string('a', 1).ToStringEx();
            var s2 = new string('a', 1).ToStringEx();

            Assert.AreSame(s1, s2);
        }

        [Test]
        public void StringEqualsTest()
        {
// ReSharper disable EqualExpressionComparison
// ReSharper disable RedundantCast
            Assert.IsTrue((StringEx)"" == (StringEx)"");
            Assert.IsTrue((StringEx)"!"==(StringEx)"!");
            Assert.IsTrue((StringEx)"123"==(StringEx)"123");
            Assert.IsTrue((StringEx)"123"!=(StringEx)"12");
            Assert.IsTrue((StringEx)"12"!=(StringEx)"123");
            Assert.IsTrue((StringEx)"12"!=(StringEx)"21");
            Assert.IsTrue((StringEx)"12"!=(StringEx)"13");
// ReSharper restore RedundantCast
// ReSharper restore EqualExpressionComparison
        }
    }
}