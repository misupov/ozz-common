﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Ozz.Common.Collections.Immutable;

namespace Ozz.Common.Tests
{
    [TestFixture]
    public class AugmentTreeTests
    {
        [Test]
        public void TestAddItemMethod()
        {
            var tree = AugmentedTree.Create<int>()
                .Add(0)
                .Add(1)
                .Add(2)
                .Add(3)
                .Add(4);
            Assert.AreEqual(0, tree[0]);
            Assert.AreEqual(1, tree[1]);
            Assert.AreEqual(2, tree[2]);
            Assert.AreEqual(3, tree[3]);
            Assert.AreEqual(4, tree[4]);
        }

        [Test]
        public void TestInsertItemMethod()
        {
            var tree = AugmentedTree.Create<int>()
                .Insert(0, 0)
                .Insert(0, 1)
                .Insert(0, 2)
                .Insert(0, 3)
                .Insert(0, 4);
            Assert.AreEqual(4, tree[0]);
            Assert.AreEqual(3, tree[1]);
            Assert.AreEqual(2, tree[2]);
            Assert.AreEqual(1, tree[3]);
            Assert.AreEqual(0, tree[4]);
        }

        [Test]
        public void TestAllModificatorMethodsAndIndexer()
        {
            // TODO: write own random algorithm for tests, because system one is platform specific
            var r = new Random(0);
            var tree = AugmentedTree.Create<int>();
            var expectedList = new List<int>();
            for (int i = 0; i < 10000; i++)
            {
                switch (r.Next(4))
                {
                    case 0:
                    {
                        var item = r.Next();
                        tree = tree.Add(item);
                        expectedList.Add(item);
                        break;
                    }
                    case 1:
                    {
                        var index = r.Next(0, expectedList.Count);
                        var item = r.Next();
                        tree = tree.Insert(index, item);
                        expectedList.Insert(index, item);
                        break;
                    }
                    case 2:
                        if (expectedList.Count > 0)
                        {
                            var index = r.Next(0, expectedList.Count);
                            var item = r.Next();
                            tree = tree.ReplaceAt(index, item);
                            expectedList[index] = item;
                        }
                        break;
                    case 3:
                        if (expectedList.Count > 0)
                        {
                            var index = r.Next(0, expectedList.Count);
                            tree = tree.RemoveAt(index);
                            expectedList.RemoveAt(index);
                        }
                        break;
                }
            }
            var actualList = new List<int>((int) tree.Count);
            for (int i = 0; i < tree.Count; i++)
            {
                actualList.Add(tree[i]);
            }
            CollectionAssert.AreEqual(expectedList, actualList);
        }
    }
}
