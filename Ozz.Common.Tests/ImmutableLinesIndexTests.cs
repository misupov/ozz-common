﻿using System.Threading;
using NUnit.Framework;
using Ozz.Common.Documents;
using Ozz.Common.Strings;

namespace Ozz.Common.Tests
{
    [TestFixture]
    public class ImmutableLinesIndexTests
    {
        [Test]
        public void AddToEndOfLineWithoutBreak()
        {
            var linesIndex = new ImmutableLinesIndex();
            linesIndex = linesIndex.Insert(0, "123", CancellationToken.None);
            linesIndex = linesIndex.Insert(3, "456", CancellationToken.None);

            // Result:
            // 123456

            Assert.AreEqual(1, linesIndex.LinesCount);

            Assert.AreEqual(6, linesIndex[0].Length);
            Assert.AreEqual(LineBreak.NoBreak, linesIndex[0].LineBreak);
        }

        [Test]
        public void AddToEndOfLineWithBreak()
        {
            var linesIndex = new ImmutableLinesIndex();
            linesIndex = linesIndex.Insert(0, "123\n", CancellationToken.None);
            linesIndex = linesIndex.Insert(4, "456", CancellationToken.None);

            // Result:
            // 123\n
            // 456

            Assert.AreEqual(2, linesIndex.LinesCount);

            Assert.AreEqual(4, linesIndex[0].Length);
            Assert.AreEqual(LineBreak.Lf, linesIndex[0].LineBreak);

            Assert.AreEqual(3, linesIndex[1].Length);
            Assert.AreEqual(LineBreak.NoBreak, linesIndex[1].LineBreak);
        }

        [Test]
        public void InsertSingleLineWithoutBreaks()
        {
            var linesIndex = new ImmutableLinesIndex();
            linesIndex = linesIndex.Insert(0, "123456", CancellationToken.None);
            linesIndex = linesIndex.Insert(3, "asd", CancellationToken.None);

            // Result:
            // 123asd456

            Assert.AreEqual(1, linesIndex.LinesCount);

            Assert.AreEqual(9, linesIndex[0].Length);
            Assert.AreEqual(LineBreak.NoBreak, linesIndex[0].LineBreak);
        }

        [Test]
        public void InsertSingleLineWithBreakAtTheBeginning()
        {
            var linesIndex = new ImmutableLinesIndex();
            linesIndex = linesIndex.Insert(0, "123456", CancellationToken.None);
            linesIndex = linesIndex.Insert(0, "asd\r\n", CancellationToken.None);

            // Result:
            // asd\r\n
            // 123456

            Assert.AreEqual(2, linesIndex.LinesCount);

            Assert.AreEqual(5, linesIndex[0].Length);
            Assert.AreEqual(LineBreak.CrLf, linesIndex[0].LineBreak);

            Assert.AreEqual(6, linesIndex[1].Length);
            Assert.AreEqual(LineBreak.NoBreak, linesIndex[1].LineBreak);
        }

        [Test]
        public void InsertThreeLinesWithoutLastBreak()
        {
            var linesIndex = new ImmutableLinesIndex();
            linesIndex = linesIndex.Insert(0, "123456", CancellationToken.None);
            linesIndex = linesIndex.Insert(3, "asd\rASD\nqwerty", CancellationToken.None);

            // Result:
            // 123asd\r
            // ASD\n
            // qwerty456

            Assert.AreEqual(3, linesIndex.LinesCount);

            Assert.AreEqual(7, linesIndex[0].Length);
            Assert.AreEqual(LineBreak.Cr, linesIndex[0].LineBreak);

            Assert.AreEqual(4, linesIndex[1].Length);
            Assert.AreEqual(LineBreak.Lf, linesIndex[1].LineBreak);

            Assert.AreEqual(9, linesIndex[2].Length);
            Assert.AreEqual(LineBreak.NoBreak, linesIndex[2].LineBreak);
        }

        [Test]
        public void InsertThreeLinesWithLastBreak()
        {
            var linesIndex = new ImmutableLinesIndex();
            linesIndex = linesIndex.Insert(0, "123456", CancellationToken.None);
            linesIndex = linesIndex.Insert(3, "asd\rASD\nqwerty\r", CancellationToken.None);

            // Result:
            // 123asd\r
            // ASD\n
            // qwerty\r
            // 456

            Assert.AreEqual(4, linesIndex.LinesCount);

            Assert.AreEqual(7, linesIndex[0].Length);
            Assert.AreEqual(LineBreak.Cr, linesIndex[0].LineBreak);

            Assert.AreEqual(4, linesIndex[1].Length);
            Assert.AreEqual(LineBreak.Lf, linesIndex[1].LineBreak);

            Assert.AreEqual(7, linesIndex[2].Length);
            Assert.AreEqual(LineBreak.Cr, linesIndex[2].LineBreak);

            Assert.AreEqual(3, linesIndex[3].Length);
            Assert.AreEqual(LineBreak.NoBreak, linesIndex[3].LineBreak);
        }

        [Test]
        public void InsertLineWithoutBreaksBetweenCrAndLf()
        {
            var linesIndex = new ImmutableLinesIndex();
            linesIndex = linesIndex.Insert(0, "123\r\n456", CancellationToken.None);
            linesIndex = linesIndex.Insert(4, "asd", CancellationToken.None);

            // Result:
            // 123\r
            // asd\n
            // 456

            Assert.AreEqual(3, linesIndex.LinesCount);

            Assert.AreEqual(4, linesIndex[0].Length);
            Assert.AreEqual(LineBreak.Cr, linesIndex[0].LineBreak);

            Assert.AreEqual(4, linesIndex[1].Length);
            Assert.AreEqual(LineBreak.Lf, linesIndex[1].LineBreak);

            Assert.AreEqual(3, linesIndex[2].Length);
            Assert.AreEqual(LineBreak.NoBreak, linesIndex[2].LineBreak);
        }

        [Test]
        public void InsertLineThatStartsWithLfBetweenCrAndLf()
        {
            var linesIndex = new ImmutableLinesIndex();
            linesIndex = linesIndex.Insert(0, "123\r\n456", CancellationToken.None);
            linesIndex = linesIndex.Insert(4, "\nasd", CancellationToken.None);

            // Result:
            // 123\r\n
            // asd\n
            // 456

            Assert.AreEqual(3, linesIndex.LinesCount);

            Assert.AreEqual(5, linesIndex[0].Length);
            Assert.AreEqual(LineBreak.CrLf, linesIndex[0].LineBreak);

            Assert.AreEqual(4, linesIndex[1].Length);
            Assert.AreEqual(LineBreak.Lf, linesIndex[1].LineBreak);

            Assert.AreEqual(3, linesIndex[2].Length);
            Assert.AreEqual(LineBreak.NoBreak, linesIndex[2].LineBreak);
        }
    }
}