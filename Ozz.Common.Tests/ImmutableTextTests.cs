﻿using NUnit.Framework;
using Ozz.Common.Strings;
using Ozz.Common.Text;

namespace Ozz.Common.Tests
{
    [TestFixture]
    public class ImmutableTextTests
    {
        [Test]
        public void AppendTest()
        {
            var text = new ImmutableText()
                .Append("123")
                .Append("45")
                .Append("678")
                .Append("")
                .Append("9");
            Assert.AreEqual("123456789", text.ToString());
        }

        [Test]
        public void SubstringTest()
        {
            var text = new ImmutableText().Append("123456");
            var substr = text.GetSubstring(0, 6);
            Assert.AreEqual((StringEx)"123456", substr);
        }
    }
}